package ru.tsc.gulin.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";

    public static final String DESCRIPTION = "Clear all tasks";
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        getTaskService().clear();
    }

}
