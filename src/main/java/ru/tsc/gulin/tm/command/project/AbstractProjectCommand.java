package ru.tsc.gulin.tm.command.project;

import ru.tsc.gulin.tm.api.service.IProjectService;
import ru.tsc.gulin.tm.command.AbstractCommand;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.model.Project;
import ru.tsc.gulin.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("START DATE: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("END DATE: " + DateUtil.toString(project.getDateEnd()));
    }

}
