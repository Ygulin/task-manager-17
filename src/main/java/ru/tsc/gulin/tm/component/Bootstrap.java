package ru.tsc.gulin.tm.component;

import ru.tsc.gulin.tm.api.repository.ICommandRepository;
import ru.tsc.gulin.tm.api.repository.IProjectRepository;
import ru.tsc.gulin.tm.api.repository.ITaskRepository;
import ru.tsc.gulin.tm.api.service.*;
import ru.tsc.gulin.tm.command.AbstractCommand;
import ru.tsc.gulin.tm.command.project.*;
import ru.tsc.gulin.tm.command.system.*;
import ru.tsc.gulin.tm.command.task.*;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.gulin.tm.exception.system.CommandNotSupportedException;
import ru.tsc.gulin.tm.model.Project;
import ru.tsc.gulin.tm.model.Task;
import ru.tsc.gulin.tm.repository.CommandRepository;
import ru.tsc.gulin.tm.repository.ProjectRepository;
import ru.tsc.gulin.tm.repository.TaskRepository;
import ru.tsc.gulin.tm.service.*;
import ru.tsc.gulin.tm.util.DateUtil;
import ru.tsc.gulin.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN**");
            }
        });
    }

    {
        registry(new InfoCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new ArgumentCommand());
        registry(new CommandsCommand());
        registry(new VersionCommand());
        registry(new ExitCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args){
        if (processArgument(args)) System.exit(0);
        initData();
        initLogger();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void initData(){
        taskService.add(new Task("Test1", Status.IN_PROGRESS, DateUtil.toDate("05.11.2019")));
        taskService.add(new Task("Test2", Status.NOT_STARTED, DateUtil.toDate("09.12.2020")));
        taskService.add(new Task("Test3", Status.COMPLETED, DateUtil.toDate("04.7.2019")));
        projectService.add(new Project("Test1", Status.COMPLETED, DateUtil.toDate("05.11.2018")));
        projectService.add(new Project("Test2", Status.IN_PROGRESS, DateUtil.toDate("03.12.2021")));
        projectService.add(new Project("Test3", Status.NOT_STARTED, DateUtil.toDate("06.10.2020")));
    }

    public void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

}
